package oceanDirectJNI;

public class OceanDirectException extends RuntimeException {

	public OceanDirectException(String message) {
		super(message);
	}
	
	public OceanDirectException(String function, int errorCode) {
		super("Error " + errorCode + " in function " + function + ": " + OceanDirect.get_error_string(errorCode));
	}
}
