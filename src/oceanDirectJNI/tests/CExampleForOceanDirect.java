package oceanDirectJNI.tests;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.DoubleBuffer;

import oceanDirectJNI.OceanDirect;
import otherSupport.SettingsManager;

public class CExampleForOceanDirect {
	static {
		System.load(SettingsManager.defaultGlobal().getPathProperty("oceanDirect.jniLibPath", "/usr/lib/oceanDirectJNI.so"));
	}
	
	public static void main(String[] args) {
		// Call initialize before using OceanDirect
	    OceanDirect.initialize();
	    
	    // Probe for Ocean devices
	    int foundDevices = OceanDirect.probe_devices();
	    System.out.println("Found " + foundDevices + " devices");
	    if (foundDevices > 0) {
	        // Start by getting the devies identifiers we will use in most function calls
	        long[] ids = new long[foundDevices];
	        int idCount = OceanDirect.get_device_ids(ids);
		    System.out.println("Got " + idCount + " devices");

		    //OceanDirect.adv_reset_device(ids[0]);
	        
	        // Open the first device detected
	        OceanDirect.open_device(ids[0]);
	        System.out.println("Opened device " + ids[0]);
	        
	        
	        // Get the serial number
	        String serial = OceanDirect.get_serial_number(ids[0]);
	        System.out.println(String.format("Found spectrometer %s", serial));

	        // Set the integration time to 100ms (100000 microsecs) and acquire a spectrum
	        int spectrumLength = OceanDirect.get_formatted_spectrum_length(ids[0]);
	        System.out.println("Spectrum length = " + spectrumLength);
	        ByteBuffer spectrum = ByteBuffer.allocateDirect(spectrumLength * 8);
	        
	        long integrationTime = 100000;
	        OceanDirect.set_integration_time_micros(ids[0], integrationTime);
	        int acquired = OceanDirect.get_formatted_spectrum(ids[0], spectrum);
	        System.out.println("Acquired " + acquired);

	        spectrum.order(ByteOrder.LITTLE_ENDIAN);
	        DoubleBuffer spectrumDbl = spectrum.asDoubleBuffer();
	        
	        // For example, find the maximum pixel intensity in the acquired spectrum
	        double maxPixelIntensity = 0.0;
	        int maxPixel = 0;
	        for (int pixel = 0; pixel < spectrumLength; ++pixel) {
	            if (spectrumDbl.get(pixel) > maxPixelIntensity) {
	                maxPixelIntensity = spectrumDbl.get(pixel);
	                maxPixel = pixel;
	            }
	        }
	        System.out.println(String.format("Maximum pixel is %d, with intensity %g", maxPixel, maxPixelIntensity));

	        // Clean up by closing the device and calling the shutdown function to free resources in OceanDirect
	        OceanDirect.close_device(ids[0]);
	        OceanDirect.shutdown();

	        // Lastly release the allocated memory
	        //free(ids);
	        //free(serial);
	        //free(spectrum);
	    }
	    else {
	    	System.out.println("No spectrometers detected");
	    }
	}
}
