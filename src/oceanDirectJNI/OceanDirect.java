package oceanDirectJNI;

import java.nio.ByteBuffer;

public class OceanDirect {

        /**
   	  * This should be called prior to any other call.  The API may
   	  * recover gracefully if this is not called, but future releases may assume
   	  * this is called first.  This should be called synchronously -- a single
   	  * thread should call this.
         */

   	public static native void initialize();

   	/**
   	 * This may be called to free up any allocated memory being held by the
   	 * driver interface.  After this is called by any thread, initialize
   	 * should be called again before any other  functions are used.
   	 */
   	public static native void shutdown();
   	
   	
    /**
     * This causes a search for known devices on all buses that support
     * autodetection.  This does NOT automatically open any device -- that must
     * still be done with the open_device() function.  Note that this
     * should only be done by one thread at a time, and it is recommended that
     * other threads avoid calling get_number_of_device_ids() or
     * get_device_ids() while this is executing.  Ideally, a single thread
     * should be designated for all device discovery/get actions, and
     * separate worker threads can be used for each device discovered.
     *
     * @return the total number of devices that have been found
     *      automatically.  If called repeatedly, this will always return the
     *      number of devices most recently found, even if they have been
     *      found or opened previously.
     */
    public static native int probe_devices();


    /**
     * This will populate the provided buffer with up to max_ids of device
     * references. These references must be used as the first parameter to
     * most of the other  calls.  Each uniquely identifies a single
     * device instance. Note that this should only be done by one thread at a time.
     * For multithreaded application this function must be synchronized.
     *
     * @param ids an array of long integers that will be overwritten
     *           with the unique IDs of each known device.  Note that these
     *           devices will not be open by default.
     * @param max_ids the maximum number of IDs that may be written
     *           to the array
     *
     * @return The total number of device IDs that were written to the array.
     *      This may be zero on error.
     */
    public static native int get_device_ids(long ids[]);


    /**
     * This function opens a device attached to the system.  The device must
     * be provided as a location ID from the get_device_ids()
     * function. Such locations can either be specified or probed using the
     * other methods in this interface. After the device is closed the id becomes invalid. You need to 
     * call either probe_devices()/add_network_devices()/detect_network_devices() and
     * get_device_ids() in order to have a valid id before reopening the device again. For a network
     * connected device this function may return an error code if the device is not yet ready to accept
     * incoming connection or the device is unreachable. Note that this should only be done by one
     * thread at a time. For multithreaded application this function must be synchronized.
     *
     * @param id The location ID of a device to try to open.  Only IDs
     *      that have been returned by a previous call to oceandirect_get_device_ids()
     *      are valid.
     * @param error_code A pointer to an integer that can be used for
     *      storing error codes:
     *                  ERROR_SUCCESS on success;
     *                  ERROR_NO_DEVICE if the device does not exist.
     */
    public static native void open_device(long id);

    /**
	 * This returns an integer denoting the number of pixels in a
	 * formatted spectrum (as returned by get_formatted_spectrum(...)).
	 *
	 * @param deviceID (Input)  The index of a device previously opened with
	 *      open_device().
	 * @param error_code (Output) pointer to an integer that can be used for
	 *      storing error codes. This may be NULL in which case no error code is returned.
	 *      The error codes returned by this function are:
	 *                  ERROR_SUCCESS on success;
	 *                  ERROR_FEATURE_NOT_FOUND if the feature does not exist;
	 *                  ERROR_TRANSFER_ERROR if a communication error occurred;
	 *                  ERROR_NO_DEVICE if the device does not exist.
	 *
	 * @return the length of a formatted spectrum.
	 */
    public static native int get_formatted_spectrum_length(long deviceID);

	/**
	 * This acquires a spectrum and returns the answer in formatted
	 *     floats.  In this mode, auto-nulling should be automatically
	 *     performed for devices that support it.
	 *
	 * @param deviceID (Input) The index of a device previously opened with
	 *      open_device().
	 * @param error_code (Output) pointer to an integer that can be used for
	 *      storing error codes. This may be NULL in which case no error code is returned.
	 *      The error codes returned by this function are:
	 *                  ERROR_SUCCESS on success;
	 *                  ERROR_FEATURE_NOT_FOUND if the feature does not exist;
	 *                  ERROR_NOT_ENOUGH_BUFFER_SPACE if the size of the buffer is smaller than the formatted spectrum length;
	 *                  ERROR_TRANSFER_ERROR if a communication error occurred;
	 *                  ERROR_NO_DEVICE if the device does not exist.
	 * @param buffer (Output) A buffer (with memory already allocated) to
	 *      hold the spectral data
	 * @param buffer_length (Input) The length of the buffer
	 *
	 * @return the number of floats read into the buffer
	 */
    public static native int get_formatted_spectrum(long deviceID, ByteBuffer buffer);

    /**
	 * This function sets the integration time for the specified device.
	 * This function should not be responsible for performing stability
	 * scans.
     * NOTE: some devices that make use of onboard functionality to perform averaging have
     * a different, larger, minimum integration time for acquisition when averaging is enabled.
     * Refer to the documentation for your spectrometer to see if this is the case.
     * The minimum integration time when averaging is enabled can be determined
     * using getMinimumAveragingIntegrationTimeMicros.
     *
	 * @param deviceID (Input) The index of a device previously opened with
	 *      open_device().
	 * @param error_code (Output) pointer to an integer that can be used for
	 *      storing error codes. This may be NULL in which case no error code is returned.
	 *      The error codes returned by this function are:
	 *                  ERROR_SUCCESS on success;
	 *                  ERROR_FEATURE_NOT_FOUND if the feature does not exist;
	 *                  ERROR_TRANSFER_ERROR if a communication error occurred;
	 *                  ERROR_INPUT_OUT_OF_BOUNDS if an invalid integration time is supplied;
     *                  ERROR_INTEGRATION_TIME_BELOW_AVERAGING_MIN if averaging is enabled and the specified
     *                                             integration time is below the minimum required for averaging
     *                                             (see get_minimum_averaging_integration_time_micros);
     *                  ERROR_NO_DEVICE if the device does not exist.
	 * @param integration_time_micros (Input) The new integration time in
	 *      units of microseconds
	 */
    public static native void set_integration_time_micros(long deviceID, long integration_time_micros);
    
    /**
     * This function closes the spectrometer attached to the system. The id becomes 
     * invalid after closing the device. Note that this should only be done by one 
     * thread at a time. For multithreaded application this function must be synchronized.
     *
     * @param id (Input) The location ID of a device previously opened with open_device().
     * @param error_code (Output) pointer to an integer that can be used for
     *      storing error codes:
     *                  ERROR_SUCCESS on success;
     *                  ERROR_NO_DEVICE if the device does not exist.
     *
     */
    public static native void close_device(long id);


    /**
    * This reads the device's serial number and fills the
    * provided array (up to the given length) with it.
    *
    * @param deviceID (Input) The index of a device previously opened with
    *      open_device().
    * @param error_code (Output) pointer to an integer that can be used for
    *      storing error codes. This may be NULL in which case no error code is returned.
    *      The error codes returned by this function are:
    *                  ERROR_SUCCESS on success;
    *                  ERROR_FEATURE_NOT_FOUND if the feature does not exist;
    *                  ERROR_TRANSFER_ERROR if a communication error occurred;
    *                  ERROR_NO_DEVICE if the device does not exist.
    * @param buffer (Output)  A pre-allocated array of characters that the
    *      serial number will be copied into
    * @param buffer_length (Input) The number of values to copy into the buffer
    *      (this should be no larger than the number of chars allocated in
    *      the buffer)
    *
    * @return the number of bytes written into the buffer
    */
    public static native String get_serial_number(long deviceID);

    /**
	 * This function returns a description of the error denoted by
	 * error_code.
	 *
	 * @param error_code (Input) The integer error code to look up.  Error codes
	 *      may not be zero, but can be any non-zero integer (positive or
	 *      negative).
	 *
	 * @return char *: A description in the form of a string that describes
	 *      what the error was or "Undefined error" if the error code is not known.
	 */
    public static native String get_error_string(int error_code);
    

	/**
	 * This function determines the correct spectrometer model name assigned.
	 * It is not based on VID or PID of device.
	 *
	 * @param id (Input) The location ID of a device previously opened with
	 *      get_device_locations().
	 * @param error_code (Output) pointer to an integer that can be used for
	 *      storing error codes.  This may be NULL in which case no error code is returned.
	 *      The error codes returned by this function are:
	 * @param buffer (Output) Pointer to a user buffer that the name will be
	 *      stored into.
	 * @param length (Input) Maximum number of bytes that may be written to the
	 *      buffer
	 *
	 * @return integral number of bytes actually written to the user buffer
	 */
	public static native String get_device_name(long id);
	

    /**
     * This function sets the trigger mode for the specified device.
     * Note that requesting an unsupported mode will result in an error.
     *
     * @param deviceID (Input) The index of a device previously opened with
     *      open_device().
     * @param error_code (Output) pointer to an integer that can be used for
     *      storing error codes. This may be NULL in which case no error code is returned.
     *      The error codes returned by this function are:
     *                  ERROR_SUCCESS on success;
     *                  ERROR_FEATURE_NOT_FOUND if the feature does not exist;
     *                  ERROR_TRANSFER_ERROR if a communication error occurred;
     *                  ERROR_NO_DEVICE if the device does not exist.
     * @param mode (Input) a trigger mode (0 = normal, 1 = software,
     *      2 = synchronization, 3 = external hardware, etc - check your
     *      particular spectrometer's Data Sheet)
     */
    public static native void set_trigger_mode(long deviceID, int mode);

    /**
     * This function gets the trigger mode for the specified device.
     * Note that requesting an unsupported mode will result in an error.
     *
     * @param deviceID (Input) The index of a device previously opened with
     *      open_device().
     * @param error_code (Output) pointer to an integer that can be used for
     *      storing error codes. This may be NULL in which case no error code is returned.
     *      The error codes returned by this function are:
     *                  ERROR_SUCCESS on success;
     *                  ERROR_FEATURE_NOT_FOUND if the feature does not exist;
     *                  ERROR_TRANSFER_ERROR if a communication error occurred;
     *                  ERROR_NO_DEVICE if the device does not exist.
     *                  ERROR_NO_DEVICE if the device does not exist.
     *                  ERROR_COMMAND_NOT_SUPPORTED device don't support this command.
     * @return the trigger mode (0 = normal, 1 = software,
     *      2 = synchronization, 3 = external hardware, etc - check your
     *      particular spectrometer's Data Sheet)
     */
    public static native int get_trigger_mode(long deviceID);

    /**
     * Set the acquisition delay in microseconds.  This may also be referred to as the
     * trigger delay.  In any event, it is the time between some event (such as a request
     * for data, or an external trigger pulse) and when data acquisition begins.
     *
     * @param deviceID (Input) The index of a device previously opened with open_device().
     * @param error_code (Output) A pointer to an integer that can be used for storing
     *        error codes. This may be NULL in which case no error code is returned.
     *      The error codes returned by this function are:
     *                  ERROR_SUCCESS on success;
     *                  ERROR_FEATURE_NOT_FOUND if the feature does not exist;
     *                  ERROR_TRANSFER_ERROR if a communication error occurred;
     *                  ERROR_NO_DEVICE if the device does not exist.
     * @param delay_usec (Input) The new delay to use in microseconds
     */
    public static native void set_acquisition_delay_microseconds(long deviceID, long delay_usec);

    /**
     * Get the acquisition delay in microseconds.  This may also be referred to as the
     * trigger delay.  In any event, it is the time between some event (such as a request
     * for data, or an external trigger pulse) and when data acquisition begins.
     *
     * Note that not all devices support reading this value back.  In these cases, the
     * returned value will be the last value sent to adv_set_acquisition_delay_microseconds().
     * If no value has been set and the value cannot be read back, this function will
     * indicate an error.
     *
     * @param deviceID (Input) The index of a device previously opened with open_device().
     * @param error_code (Output) A pointer to an integer that can be used for storing
     *        error codes. This may be NULL in which case no error code is returned.
     *      The error codes returned by this function are:
     *                  ERROR_SUCCESS on success;
     *                  ERROR_FEATURE_NOT_FOUND if the feature does not exist;
     *                  ERROR_COMMAND_NOT_SUPPORTED if a communication error occurred;
     *                  ERROR_NO_DEVICE if the device does not exist.
     * @return The acquisition delay in microseconds
     */
    public static native long get_acquisition_delay_microseconds(long deviceID);

    /**
     * Get the maximum allowed acquisition delay in microseconds.
     *
     * @param deviceID (Input) The index of a device previously opened with open_device().
     * @param error_code (Output) A pointer to an integer that can be used for storing
     *        error codes. This may be NULL in which case no error code is returned.
     *      The error codes returned by this function are:
     *                  ERROR_SUCCESS on success;
     *                  ERROR_FEATURE_NOT_FOUND if the feature does not exist;
     *                  ERROR_TRANSFER_ERROR if a communication error occurred;
     *                  ERROR_NO_DEVICE if the device does not exist.
     * @return The maximum acquisition delay in microseconds
     */
    public static native long get_acquisition_delay_maximum_microseconds(long deviceID);

    /**
     * Get the minimum allowed acquisition delay in microseconds.
     *
     * @param deviceID (Input) The index of a device previously opened with open_device().
     * @param error_code (Output) A pointer to an integer that can be used for storing
     *        error codes. This may be NULL in which case no error code is returned.
     *      The error codes returned by this function are:
     *                  ERROR_SUCCESS on success;
     *                  ERROR_FEATURE_NOT_FOUND if the feature does not exist;
     *                  ERROR_TRANSFER_ERROR if a communication error occurred;
     *                  ERROR_NO_DEVICE if the device does not exist.
     * @return The minimum acquisition delay in microseconds
     */
    public static native long get_acquisition_delay_minimum_microseconds(long deviceID);

    /**
     * This function gets the integration time from the specified device.
     * This function should not be responsible for performing stability
     * scans.
     *
     * @param deviceID (Input) The index of a device previously opened with
     *      open_device().
     * @param error_code (Output) pointer to an integer that can be used for
     *      storing error codes. This may be NULL in which case no error code is returned.
     *      The error codes returned by this function are:
     *                  ERROR_SUCCESS on success;
     *                  ERROR_FEATURE_NOT_FOUND if the feature does not exist;
     *                  ERROR_TRANSFER_ERROR if a communication error occurred;
     *                  ERROR_INPUT_OUT_OF_BOUNDS if an invalid integration time is supplied;
     *                  ERROR_NO_DEVICE if the device does not exist.
     * @param integration_time_micros (Input) The new integration time in
     *      units of microseconds
     */
    public static native long get_integration_time_micros(long deviceID);

	/**
	 * This function returns the smallest integration time setting,
	 * in microseconds, that is valid for the spectrometer.
     * 
     * @note some devices that make use of onboard functionality to perform averaging have
     * a different, larger, minimum integration time for acquisition when averaging is enabled.
     * Refer to the documentation for your spectrometer to see if this is the case.
     * The minimum integration time when averaging is enabled can be determined
     * using get_minimum_averaging_integration_time_micros.
     *
	 * @param deviceID (Input) The index of a device previously opened with
	 *      open_device().
	 * @param error_code (Output) A pointer to an integer that can be used
	 *      for storing error codes. This may be NULL in which case no error code is returned.
	 *      The error codes returned by this function are:
	 *                  ERROR_SUCCESS on success;
	 *                  ERROR_FEATURE_NOT_FOUND if the feature does not exist;
	 *                  ERROR_TRANSFER_ERROR if a communication error occurred;
	 *                  ERROR_NO_DEVICE if the device does not exist.
	 * @return Returns minimum legal integration time in microseconds if > 0.
	 *      On error, returns -1 and error_code will be set accordingly.
	 */
	public static native long get_minimum_integration_time_micros(long deviceID);

    
	/**
	* This function returns the largest integration time setting,
	* in microseconds, that is valid for the spectrometer.
	*
	* @param deviceID (Input) The index of a device previously opened with
	*      open_device().
	* @param error_code (Output) A pointer to an integer that can be used
	*      for storing error codes. This may be NULL in which case no error code is returned.
	 *      The error codes returned by this function are:
	 *                  ERROR_SUCCESS on success;
	 *                  ERROR_FEATURE_NOT_FOUND if the feature does not exist;
	 *                  ERROR_TRANSFER_ERROR if a communication error occurred;
	 *                  ERROR_NO_DEVICE if the device does not exist.
	* @return Returns minimum legal integration time in microseconds if > 0.
	*      On error, returns -1 and error_code will be set accordingly.
	*/
	public static native long get_maximum_integration_time_micros(long deviceID);


    /**
     * Read a maximum of 15 spectra from the data buffer. This function requires that both back to back scans
     * and data buffer be enabled. See "adv_set_data_buffer_enable()" and "adv_set_number_of_backtoback_scans()".
     * @param deviceID (Input) The index of a device previously opened with open_device().
     * @param error_code (Output) A pointer to an integer that can be used for storing error codes.
	 *      This may be NULL in which case no error code is returned.
	 *      The error codes returned by this function are:
	 *                  ERROR_SUCCESS on success;
	 *                  ERROR_CODE_INVALID_ARGUMENT if either buffer or tick_count are a null pointer;
	 *                  ERROR_NOT_ENOUGH_BUFFER_SPACE if the buffer space size or the tick count size are not = 15;
	 *                  ERROR_FEATURE_NOT_FOUND if the feature does not exist;
	 *                  ERROR_TRANSFER_ERROR if a communication error occurred;
	 *                  ERROR_NO_DEVICE if the device does not exist.
     * @param buffer (Output) An array of pointer for storing spectra.
     * @param buffer_row_size  (Input) The buffer array size must be 15. This is the maximum value that can be return by this function.
     * @param buffer_column_size  (Input) The data size of each element of the "buffer".
     * @param tick_count (Output) The timestamp of each spectra.
     * @param tick_count_size  (Input) The "tick_count" array size must be 15. This is the maximum value that can be return by this function.
     * @return Returns the number of spectra read. It can be zero.
     */
    //public static native int get_raw_spectrum_with_metadata(long deviceID, ByteBuffer buffer, int buffer_row_size, int buffer_column_size,
    //                                        long[] tick_count, int tick_count_size);


	/**
	 * This computes the wavelengths for the spectrometer and fills in the
	 * provided array (up to the given length) with those values.
	 *
	 * @param deviceID (Input) The index of a device previously opened with
	 *      open_spectrometer().
	 * @param error_code (Ouput) pointer to an integer that can be used for storing
	 *      error codes. This may be NULL in which case no error code is returned.
	 *      The error codes returned by this function are:
	 *                  ERROR_SUCCESS on success;
	 *                  ERROR_FEATURE_NOT_FOUND if the feature does not exist;
	 *                  ERROR_TRANSFER_ERROR if a communication error occurred;
	 *                  ERROR_NO_DEVICE if the device does not exist.
	 * @param wavelengths (Output) A pre-allocated array of doubles that the wavelengths
	 *      will be copied into
	 * @param length (Input) The number of values to copy into the wavelength array
	 *      (this should be no larger than the number of doubles allocated in the wavelengths
	 *      array)
	 *
	 * @return the number of wavelengths written into the wavelength buffer
	 */
	public static native int get_wavelengths(long deviceID, double[] wavelengths);

	/**
	 * This function reads the actual temperature of the TEC and returns the value in
	 * degrees celsius.
	 *
	 * @param deviceID (Input) The index of a device previously opened with open_device().
	 * @param error_code (Output) A pointer to an integer that can be used for storing
	 *      error codes. This may be NULL in which case no error code is returned.
	 *      The error codes returned by this function are:
	 *                  ERROR_SUCCESS on success;
	 *                  ERROR_FEATURE_NOT_FOUND if the feature does not exist;
     *                  ERROR_TRANSFER_ERROR if a communication error occurred;
     *                  ERROR_COMMAND_NOT_SUPPORTED  this command is not supported by the device;
     *                  ERROR_NO_DEVICE if the device does not exist.
	 *
	 * @return The TEC temperature in degrees celsius.
	 */
	public static native double adv_tec_get_temperature_degrees_C(long deviceID);

    /**
    * Restarts the device.
    *
    * @param deviceID the ID of the device returned by getDeviceIDs.
    * @param errorCode a code indicating the result of the operation:
    *                  ERROR_SUCCESS on success;
    *                  ERROR_NO_DEVICE if the device does not exist;
    *                  ERROR_FEATURE_NOT_FOUND the feature is not enabled on the specified device;
    *                  ERROR_TRANSFER_ERROR the device protocol for the feature could not be found;
    *                  ERROR_CODE_INVALID_ARGUMENT the interface number is not 0 or 1.
    */
    public static native void adv_reset_device(long deviceID);

}
