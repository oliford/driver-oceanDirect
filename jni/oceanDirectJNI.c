#include <stdlib.h>
#include <string.h>
#include "oceanDirectJNI_OceanDirect.h"

#include "api/OceanDirectAPI.h"
#include "api/OceanDirectAPIConstants.h"


/* Check return code from Andor V2 SDK functions and throw exception if necessary */
void checkReturn(JNIEnv *env, const char *funcName, int returnCode){
	if(returnCode == ERROR_SUCCESS)
		return; //all OK
	fprintf(stderr, "checkReturn(%s) code=%i\n", funcName, returnCode); fflush(stderr);
	

	// Find and instantiate the exception
	jclass exClass = (*env)->FindClass(env, "oceanDirectJNI/OceanDirectException");

	jmethodID constructor = (*env)->GetMethodID(env, exClass, "<init>", "(Ljava/lang/String;I)V");
	if(constructor == NULL){
		fprintf(stderr, "*** JNI: CANNOT FIND EXCEPTION CONSTRUCTOR ***");
		return;
	}
	jstring jStr = (*env)->NewStringUTF(env, funcName);
	jobject exception = (*env)->NewObject(env, exClass, constructor, jStr, returnCode);

	// Throw the exception. Since this is native code,
	// execution continues, and the execution will be abruptly
	// interrupted at the point in time when we return to the VM. 
	// The calling code will perform the early return back to Java code.
	(*env)->Throw(env, (jthrowable) exception);

	// Clean up local reference
	(*env)->DeleteLocalRef(env, exClass);
}

/*
 * Class:     oceanDirectJNI_OceanDirect
 * Method:    initialize
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_oceanDirectJNI_OceanDirect_initialize
  (JNIEnv *env, jclass cls){

	odapi_initialize();

}

/*
 * Class:     oceanDirectJNI_OceanDirect
 * Method:    shutdown
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_oceanDirectJNI_OceanDirect_shutdown
  (JNIEnv *env, jclass cls){

	odapi_shutdown();
}

/*
 * Class:     oceanDirectJNI_OceanDirect
 * Method:    probe_devices
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_oceanDirectJNI_OceanDirect_probe_1devices
  (JNIEnv *env, jclass cls){
	return odapi_probe_devices();
}

/*
 * Class:     oceanDirectJNI_OceanDirect
 * Method:    get_device_ids
 * Signature: ([J)I
 */
JNIEXPORT jint JNICALL Java_oceanDirectJNI_OceanDirect_get_1device_1ids
  (JNIEnv *env, jclass cls, jlongArray jIds){

	long *cArr = (*env)->GetLongArrayElements(env, jIds, 0);
	int len = (*env)->GetArrayLength(env, jIds);
	
	int ret = odapi_get_device_ids(cArr, len);

	(*env)->ReleaseLongArrayElements(env, jIds, cArr, 0);

	return ret;
}

/*
 * Class:     oceanDirectJNI_OceanDirect
 * Method:    open_device
 * Signature: (J)I
 */
JNIEXPORT void JNICALL Java_oceanDirectJNI_OceanDirect_open_1device
  (JNIEnv *env, jclass cls, jlong deviceID){
	int error_code = 0;
	odapi_open_device(deviceID, &error_code);
	checkReturn(env, "open_device", error_code);

}

/*
 * Class:     oceanDirectJNI_OceanDirect
 * Method:    get_formatted_spectrum_length
 * Signature: (J[I)I
 */
JNIEXPORT jint JNICALL Java_oceanDirectJNI_OceanDirect_get_1formatted_1spectrum_1length
  (JNIEnv *env, jclass cls, jlong deviceID){
	
	int error_code = -1;
	int spectrumLength = odapi_get_formatted_spectrum_length(deviceID, &error_code);
	checkReturn(env, "odapi_get_formatted_spectrum_length", error_code);

	return spectrumLength;
}

/*
 * Class:     oceanDirectJNI_OceanDirect
 * Method:    get_formatted_spectrum
 * Signature: (J[ILjava/nio/ByteBuffer;)I
 */
JNIEXPORT jint JNICALL Java_oceanDirectJNI_OceanDirect_get_1formatted_1spectrum
  (JNIEnv *env, jclass cls, jlong deviceID, jobject jByteBuffer){

	jbyte *cBufferPtr = (jbyte *)(*env)->GetDirectBufferAddress(env, jByteBuffer);
	if(cBufferPtr == NULL){ 
		fprintf(stderr, "ERROR: get_formatted_spectrum(): Couldn't get ByteBuffer memory in JNI land. Is it a direct buffer?");
		fflush(stderr);
		return -1;
	}
	long bufferLen = (*env)->GetDirectBufferCapacity(env, jByteBuffer);

	int error_code = -1;
	int pixelsAcquired = odapi_get_formatted_spectrum(deviceID, &error_code, (double*)cBufferPtr, bufferLen/sizeof(double));
	checkReturn(env, "odapi_get_formatted_spectrum", error_code);

	return pixelsAcquired;
}

/*
 * Class:     oceanDirectJNI_OceanDirect
 * Method:    set_integration_time_micros
 * Signature: (JJ)I
 */
JNIEXPORT void JNICALL Java_oceanDirectJNI_OceanDirect_set_1integration_1time_1micros
  (JNIEnv *env, jclass cls, jlong deviceID, jlong integration_time_micros){

	int error_code = -1;

	odapi_set_integration_time_micros(deviceID, &error_code, (unsigned long)integration_time_micros);
	checkReturn(env, "odapi_set_integration_time_micros", error_code);

}

/*
 * Class:     oceanDirectJNI_OceanDirect
 * Method:    close_device
 * Signature: (J)I
 */
JNIEXPORT void JNICALL Java_oceanDirectJNI_OceanDirect_close_1device
  (JNIEnv *env, jclass cls, jlong deviceID){

	int error_code = -1;

        odapi_close_device(deviceID, &error_code);
	checkReturn(env, "odapi_close_device", error_code);

}

/*
 * Class:     oceanDirectJNI_OceanDirect
 * Method:    get_serial_number
 * Signature: (J[I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_oceanDirectJNI_OceanDirect_get_1serial_1number
  (JNIEnv *env, jclass cls, jlong deviceID){

	int error_code = -1;
	unsigned char maxLen = odapi_get_serial_number_maximum_length(deviceID, &error_code);

	checkReturn(env, "odapi_get_serial_number_maximum_length", error_code);
	if(error_code != ERROR_SUCCESS){
		return NULL;
	}

	fprintf(stderr, "calling malloc(%i)\n", maxLen); fflush(stderr);
	char *cStr = (char *)malloc(maxLen+1);
	if(cStr == NULL){
		fprintf(stderr, "ERROR: get_serial_number(): malloc(%i) failed.\n", maxLen);
		fflush(stderr);
		//use an error code that sounds a bit like a memory error
		checkReturn(env, "get_serial_number.malloc", ERROR_NOT_ENOUGH_BUFFER_SPACE);
		return NULL;
	}

	fprintf(stderr, "calling odapi_get_serial_number(%p, %i)\n", cStr, maxLen); fflush(stderr);
        int strLen = odapi_get_serial_number(deviceID, &error_code, cStr, maxLen);
//	error_code = 0;
//	strcpy(cStr, "MERP");
//	int strLen = 4;
	fprintf(stderr, "odapi_get_serial_number return %i code=%i\n", strLen, error_code); fflush(stderr);

	checkReturn(env, "odapi_get_serial_number", error_code);
        if(error_code != ERROR_SUCCESS){
		return NULL;
	}
	cStr[strLen] = 0x00;

	jstring jRetString = (*env)->NewStringUTF(env, cStr);


	fprintf(stderr, "free\n"); fflush(stderr);
	free(cStr);

	fprintf(stderr, "return %p\n", jRetString); fflush(stderr);
	return jRetString;
}



/*
 * Class:     oceanDirectJNI_OceanDirect
 * Method:    get_error_string
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_oceanDirectJNI_OceanDirect_get_1error_1string
  (JNIEnv *env, jclass cls, jint error_code){

	
	int len = odapi_get_error_string_length(error_code);

	char *cStr = (char *)malloc(len+1);
	if(cStr == NULL){
		fprintf(stderr, "ERROR: get_error_string(): malloc(%i) failed.\n", len);
		//throwing an exception will just get us in a loop, so give up
		return NULL;
	}

        int strLen = odapi_get_error_string(error_code, cStr, len);

	cStr[strLen] = 0x00;

	jstring jRetString = (*env)->NewStringUTF(env, cStr);

	free(cStr);

	return jRetString;
}

/*
 * Class:     oceanDirectJNI_OceanDirect
 * Method:    get_device_name
 * Signature: (J)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_oceanDirectJNI_OceanDirect_get_1device_1name
  (JNIEnv *env, jclass cls, jlong deviceID){
	

	int len = 1023;

	char *cStr = (char *)malloc(len+1);
	if(cStr == NULL){
		fprintf(stderr, "ERROR: get_device_name(): malloc(%i) failed.\n", len); fflush(stderr);
		//throwing an exception will just get us in a loop, so give up
		return NULL;
	}
	fprintf(stderr, "get_device_name(): malloc(%i) ok.\n", len); fflush(stderr);

	int error_code = -1;
        int strLen = odapi_get_device_name(deviceID, &error_code, cStr, len);
	checkReturn(env, "odapi_get_device_name", error_code);
	fprintf(stderr,"odapi_get_device_name = %i\n", strLen); fflush(stderr);

	if(strLen >= len){
		fprintf(stderr,"Truncating device name to %i\n", len-1); fflush(stderr);
		strLen = len-1;  //truncate to avoid overrun
	}

	cStr[strLen] = 0x00;

	jstring jRetString = (*env)->NewStringUTF(env, cStr);

	free(cStr);

	return jRetString;	

}

/*
 * Class:     oceanDirectJNI_OceanDirect
 * Method:    set_trigger_mode
 * Signature: (JI)V
 */
JNIEXPORT void JNICALL Java_oceanDirectJNI_OceanDirect_set_1trigger_1mode
  (JNIEnv *env, jclass cls, jlong deviceID, jint mode){

	int error_code = -1;

        odapi_set_trigger_mode(deviceID, &error_code, mode);
	checkReturn(env, "odapi_set_trigger_mode", error_code);

}

/*
 * Class:     oceanDirectJNI_OceanDirect
 * Method:    get_trigger_mode
 * Signature: (J)I
 */
JNIEXPORT jint JNICALL Java_oceanDirectJNI_OceanDirect_get_1trigger_1mode
  (JNIEnv *env, jclass cls, jlong deviceID){

	int error_code = -1;

	int mode = odapi_get_trigger_mode(deviceID, &error_code);
	checkReturn(env, "odapi_get_trigger_mode", error_code);

	return mode;
}

/*
 * Class:     oceanDirectJNI_OceanDirect
 * Method:    set_acquisition_delay_microseconds
 * Signature: (JJ)V
 */
JNIEXPORT void JNICALL Java_oceanDirectJNI_OceanDirect_set_1acquisition_1delay_1microseconds
  (JNIEnv *env, jclass cls, jlong deviceID, jlong delay){

	int error_code = -1;

        odapi_set_acquisition_delay_microseconds(deviceID, &error_code, delay);
	checkReturn(env, "odapi_set_acquisition_delay_microseconds", error_code);

}


/*
 * Class:     oceanDirectJNI_OceanDirect
 * Method:    get_acquisition_delay_microseconds
 * Signature: (J)J
 */
JNIEXPORT jlong JNICALL Java_oceanDirectJNI_OceanDirect_get_1acquisition_1delay_1microseconds
  (JNIEnv *env, jclass cls, jlong deviceID){

	int error_code = -1;

        long delay = odapi_get_acquisition_delay_microseconds(deviceID, &error_code);
	checkReturn(env, "odapi_get_acquisition_delay_microseconds", error_code);

	return delay;
}

/*
 * Class:     oceanDirectJNI_OceanDirect
 * Method:    get_acquisition_delay_maximum_microseconds
 * Signature: (J)J
 */
JNIEXPORT jlong JNICALL Java_oceanDirectJNI_OceanDirect_get_1acquisition_1delay_1maximum_1microseconds
  (JNIEnv *env, jclass cls, jlong deviceID){

	int error_code = -1;

        long maxDelay = odapi_get_acquisition_delay_maximum_microseconds(deviceID, &error_code);
	checkReturn(env, "odapi_get_acquisition_delay_maximum_microseconds", error_code);

	return maxDelay;
}


/*
 * Class:     oceanDirectJNI_OceanDirect
 * Method:    get_acquisition_delay_minimum_microseconds
 * Signature: (J)J
 */
JNIEXPORT jlong JNICALL Java_oceanDirectJNI_OceanDirect_get_1acquisition_1delay_1minimum_1microseconds
  (JNIEnv *env, jclass cls, jlong deviceID){

	int error_code = -1;

        long minDelay = odapi_get_acquisition_delay_minimum_microseconds(deviceID, &error_code);
	checkReturn(env, "odapi_get_acquisition_delay_minimum_microseconds", error_code);

	return minDelay;
}

/*
 * Class:     oceanDirectJNI_OceanDirect
 * Method:    get_integration_time_micros
 * Signature: (J)J
 */
JNIEXPORT jlong JNICALL Java_oceanDirectJNI_OceanDirect_get_1integration_1time_1micros
  (JNIEnv *env, jclass cls, jlong deviceID){

	int error_code = -1;

        long expTime = odapi_get_integration_time_micros(deviceID, &error_code);
	checkReturn(env, "odapi_get_integration_time_micros", error_code);

	return expTime;
}

/*
 * Class:     oceanDirectJNI_OceanDirect
 * Method:    get_minimum_integration_time_micros
 * Signature: (J)J
 */
JNIEXPORT jlong JNICALL Java_oceanDirectJNI_OceanDirect_get_1minimum_1integration_1time_1micros
  (JNIEnv *env, jclass cls, jlong deviceID){

	int error_code = -1;

        long minExpTime = odapi_get_minimum_integration_time_micros(deviceID, &error_code);
	checkReturn(env, "odapi_get_minimum_integration_time_micros", error_code);

	return minExpTime;
}


/*
 * Class:     oceanDirectJNI_OceanDirect
 * Method:    get_maximum_integration_time_micros
 * Signature: (J)J
 */
JNIEXPORT jlong JNICALL Java_oceanDirectJNI_OceanDirect_get_1maximum_1integration_1time_1micros
  (JNIEnv *env, jclass cls, jlong deviceID){

	int error_code = -1;

        long maxExpTime = odapi_get_maximum_integration_time_micros(deviceID, &error_code);
	checkReturn(env, "odapi_get_maximum_integration_time_micros", error_code);

	return maxExpTime;
}

/*
 * Class:     oceanDirectJNI_OceanDirect
 * Method:    get_raw_spectrum_with_metadata
 * Signature: (JLjava/nio/ByteBuffer;II[JI)I
 */
/*JNIEXPORT jint JNICALL Java_oceanDirectJNI_OceanDirect_get_1raw_1spectrum_1with_1metadata
  (JNIEnv *env, jclass cls, jlong deviceID, jobject jByteBuffer, jint buffer_row_size, jint buffer_column_size, 
			jlongArray jTickCountArray, jint tick_count_size){

	jbyte *cBufferPtr = (jbyte *)(*env)->GetDirectBufferAddress(env, jByteBuffer);
	if(cBufferPtr == NULL){ 
		fprintf(stderr, "ERROR: get_raw_spectrum_with_metadata(): Couldn't get ByteBuffer memory in JNI land. Is it a direct buffer?");
		fflush(stderr);
		return -1;
	}
	long bufferLen = (*env)->GetDirectBufferCapacity(env, jByteBuffer);

	int error_code = -1;
	int pixelsAcquired = odapi_get_raw_spectrum_with_metadata(deviceID, &error_code, (double*)cBufferPtr, 
							buffer_row_size, buffer_column_size, tick_count, tick_count_size);

	checkReturn(env, "get_raw_spectrum_with_metadata", error_code);

	return pixelsAcquired;
}*/

/*
 * Class:     oceanDirectJNI_OceanDirect
 * Method:    get_wavelengths
 * Signature: (J[D)I
 */
JNIEXPORT jint JNICALL Java_oceanDirectJNI_OceanDirect_get_1wavelengths
  (JNIEnv *env, jclass cls, jlong deviceID, jdoubleArray jWavelengthArray){

	double *cArr = (*env)->GetDoubleArrayElements(env, jWavelengthArray, 0);
	int len = (*env)->GetArrayLength(env, jWavelengthArray);
	
	int error_code = -1;

	int nRet = odapi_get_wavelengths(deviceID, &error_code, cArr, len);
	checkReturn(env, "odapi_get_wavelengths", error_code);

	(*env)->ReleaseDoubleArrayElements(env, jWavelengthArray, cArr, 0);

	return nRet;
}

/*
 * Class:     oceanDirectJNI_OceanDirect
 * Method:    adv_tec_get_temperature_degrees_C
 * Signature: (J)D
 */
JNIEXPORT jdouble JNICALL Java_oceanDirectJNI_OceanDirect_adv_1tec_get_1temperature_1degrees_1C
  (JNIEnv *env, jclass cls, jlong deviceID){

	int error_code = -1;

        double temp = odapi_adv_tec_get_temperature_degrees_C(deviceID, &error_code);
	checkReturn(env, "odapi_adv_tec_get_temperature_degrees_C", error_code);

	return temp;
}

/*
 * Class:     oceanDirectJNI_OceanDirect
 * Method:    adv_reset_device
 * Signature: (J)V
 */
JNIEXPORT void JNICALL Java_oceanDirectJNI_OceanDirect_adv_1reset_1device
  (JNIEnv *env, jclass cls, jlong deviceID){

	int error_code = -1;

        odapi_adv_reset_device(deviceID, &error_code);
	checkReturn(env, "odapi_adv_reset_device", error_code);

}

